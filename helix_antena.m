fc       = 0.868e9; %carrier frequency 868Mhz
lambda    = physconst('LightSpeed')/fc;
r           = 0.3e-3;
width       = cylinder2strip(r);
feedheight  = 3*r;

%D    = 93000/fc;
D           = 90e-3;
radius      = D/2;
turns       = 15;
pitch       = 14;
spacing     = 0.24*lambda;%helixpitch2spacing(pitch,radius);
%side        = 600e-3;
%radiusGP    = side/2;
radiusGP    =  0.62*lambda;

relativeBW  = 0.45;
BW          = relativeBW*fc;

hx = helix('Radius',radius,'Width',width,'Turns',turns,...
           'Spacing',spacing,'GroundPlaneRadius',radiusGP,...
           'FeedStubHeight',feedheight);
figure;
pattern(hx,fc);
%show(hx);